'use strict';

/* Services */

var yaigServices = angular.module('yaigServices', ['ngResource']);

yaigServices.factory('Test', ['$resource',
  function($resource){
    return $resource('phones/:testId', {}, {
      query: {method:'GET', params:{testId:''}, isArray:true}
    });
  }]);
