'use strict';

/* App Module */

var yaigApp = angular.module('yaigApp', [
    'ngRoute',
    'yaigControllers',
    'yaigServices'
]);

yaigApp.config(['$routeProvider',
  function($routeProvider) {
    $routeProvider.
      when('/tests', {
        templateUrl: 'partials/test-list.html',
        controller: 'TestListCtrl'
      }).
      when('/tests/:testId', {
        templateUrl: 'partials/test-detail.html',
        controller: 'TestDetailCtrl'
      }).
      otherwise({
        redirectTo: '/tests'
      });
  }]);
