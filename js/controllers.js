'use strict';

/* Controllers */

var yaigControllers = angular.module('yaigControllers', ['ui.bootstrap']);

yaigControllers.controller('TestListCtrl', ['$scope', 'Test',
    function($scope, Test) {
        $scope.Title = "TestHeading";
        $scope.subTitle = "TestSubTitle";
    }
]);

yaigControllers.controller('TestDetailCtrl', ['$scope', '$routeParams', 'Test',
    function($scope, $routeParams, Test) {
        
    }
]);